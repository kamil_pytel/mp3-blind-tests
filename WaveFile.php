<?php
class WaveFile
{
    protected $file;
    protected $filesize;
    protected $formatInfo;
    protected $fmtBlockStart;
    public function __construct($filepath)
    {
        $this->file = fopen($filepath, 'r');
        $this -> filesize = filesize($filepath);
        $this -> parseHeaders();
    }

    public function parseHeaders()
    {
        $this -> blocks = [];
        fseek($this -> file, 0);
        $fileFormat = fread($this -> file, 4);
    #	$headersLen =  unpack("H", fread($this -> file, 4));
        $dataLen = $this -> binToInt(fread($this -> file, 4));
        #$this -> assertEqual($dataLen+4, $this -> filesize, "File size doesnt match RIFF headers");
        $format2 = fread($this -> file, 4);
        $this -> fmtBlockStart = ftell($this -> file);
        $this -> assertEqual($format2, "WAVE", "File codec not supported");
        while ($block = $this -> nextBlock()) {
            $this -> blocks[$block['name']] = $block;
        }
    }
    public function getFormatInfo()
    {
        if (!$this -> formatInfo) {
            $fmtBytes = $this -> readFmtBlock();
            $format = new FormatInfo();
            $format -> audioFormat = $this -> binToInt($fmtBytes[0]);
            $format -> numberOfChannels = $this -> binToInt($fmtBytes[1]);
            $format -> sampleRate = $this -> binToInt($fmtBytes[2].$fmtBytes[3]);
            $format -> byteRate = $this -> binToInt($fmtBytes[4].$fmtBytes[5]);
            $format -> bytesPerSample = $this -> binToInt($fmtBytes[6]);
			$format -> bitsPerSample = $this -> binToInt($fmtBytes[7]);
			$this -> formatInfo = $format;
        }
        return $this -> formatInfo;
    }
    protected function assertEqual($value1, $value2, $message)
    {
        if ($value1 !== $value2) {
            throw new Exception("Assertion failed: [$value1 != $value2]".$message);
        }
    }
    protected function binToInt($bin)
    {
        return hexdec(implode('', array_reverse(str_split(bin2hex($bin), 2))));
    }
    public function readFmtBlock()
    {
        $fmtBlockContent = $this -> readBlock($this -> fmtBlockStart);
        $fmtBytes = str_split($fmtBlockContent, 2);
        return $fmtBytes;
    }
    public function readBlock($start = null)
    {
        if ($start !== null) {
            fseek($this->file, $start);
        }
        $blockName = $this -> readBytes(4);//fread($this -> file, 4);
        $blockSize = $this -> readInt();//$this -> binToInt(fread($this -> file, 4));
        return $this -> readBytes($blockSize);
    }
    public function nextBlock()
	{
		$blockStart = ftell($this -> file);
        $blockName = $this -> readBytes(4); 
        $blockSize = $this -> readInt(); 
        if ($blockName && $blockSize) {
            fseek($this->file, $blockSize, SEEK_CUR);
            return ['name' => $blockName, 'size' => $blockSize, 'start' => $blockStart];
        } else {
            return false;
        }
	}
	public function readBytes($bytes = 4) {
		return fread($this -> file, $bytes);
	}
	public function readInt($size = 4) {
		return $this -> bin2unsigned(fread($this -> file, $size));
	}
	public function readSignedInt() {
		return $this -> bin2signed($this -> readBytes(2));
	}
	public function bin2signed($bin) {
		$repacked = hexdec(bin2hex(strrev($bin)));
		$maxValue = (int)floor((pow(2, strlen($bin)*8)-1)/2);
		if ($repacked > $maxValue) { 
			$repacked -= 65536;
		}
		return $repacked;
	}

	public function bin2unsigned($bin) {
		$repacked = hexdec(bin2hex(strrev($bin)));
		return $repacked;
	}
	public function estimateLength() {
		$format = $this -> getFormatInfo();
		$bytesPerSecond = $format -> bytesPerSample*$format -> sampleRate;
		$dataLength = $this -> blocks['data']['size'];
		return $dataLength/$bytesPerSecond;
	}
	public function resample($requestedSamples, $method = "avg") {
		$format = $this -> getFormatInfo();
		$newSamples = [];
		fseek($this -> file, $this -> blocks['data']['start']+8);//after subchunk headers
		$samplesCount = ($this -> blocks['data']['size']/$format -> bytesPerSample);
		$samplesPerNewSample = ceil($samplesCount/$requestedSamples);
		$bytesToRead = ceil($format -> bitsPerSample/8);
		for($resampledCount=0;$resampledCount<$requestedSamples;++$resampledCount) {
			set_time_limit(0);
			$tmp = [];
			for ($i = 0;$i<$samplesPerNewSample;++$i) {
				for($channel=0;$channel < $format->numberOfChannels;$channel++) {
					$int = $this -> readSignedInt(); 
					$tmp[$channel][] = $int;
				}
			}
			$sample = $this -> {'resample_'.$method}($tmp);
			yield $sample;
			//$newSamples[] = $sample;
		}
		//return $newSamples;
	}
	protected function resample_avg($chunk) {
		$sum_positive = 0;
		$sum_negative = 0;
		$cnt_positive = 0;
		$cnt_negative = 0;
		foreach ($chunk as $channel) {
			foreach ($channel as $sample) {
				if ($sample >= 0) {
					$sum_positive += $sample;
					++$cnt_positive;
				} 
				if ($sample <= 0) {
					$sum_negative += $sample;
					++$cnt_negative;
				} 
			}
		}
		return [
			'positive' => $sum_positive/$cnt_positive,
			'negative' => $sum_negative/$cnt_negative
		];
	}
	protected function resample_ext($chunk) {
		$max_positive = 0;
		$max_negative = 0;
		foreach ($chunk as $channel) {
			foreach ($channel as $sample) {
				if ($sample > 0) {
					if ($sample > $max_positive) {
						$max_positive = $sample;
					}
				} 
				if ($sample < 0) {
					if ($sample < $max_negative) {
						$max_negative = $sample;
					}
				} 
			}
		}
		return [
			'positive' => $max_positive,
			'negative' => $max_negative
		];
	}
}

class FormatInfo
{
    const FORMAT_PCM = 1;
    public $audioFormat;

    public $numberOfChannels;
    public $sampleRate;
    public $byteRate;
    public $bytesPerSample;
    public $bitsPerSample;
}
